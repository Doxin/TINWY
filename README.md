![Screenshot of TINWY](https://raw.githubusercontent.com/SuperDoxin/TINWY/master/img/screenshot.png)

# TINWY

There Is No Wind Yet powder simulation game. a feeble attempt at writing a
powder simulation game. for a propper game in this genre have a look at
[The Powder Toy](http://powdertoy.co.uk/).

## Currently implemented

- simple dust particle that falls down according to gravity.
- simple wall particle that doesn't fall down according to gravity
- a spout at the top of the screen that makes dust

## Setting up a development enviroment

- download and install [DMD](http://dlang.org/download.html)
- download and install [DUB](http://code.dlang.org/download)
- `git clone https://github.com/SuperDoxin/TINWY.git`
- `cd TINWY`

## Compiling and running

- `dub build` to compile
- `dub run` to compile and build

