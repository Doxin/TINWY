private import settings;
private import io=std.stdio;
private import std.typecons;

Particle[H][W] particlegrid;
Particle*[] active_particles;
Particle*[] paused_particles;
Particle*[] sleeping_particles;
uint screen[W*H];

enum ParticleState{ACTIVE,PAUSED,SLEEPING}

struct Color
    {
    ubyte r;
    ubyte g;
    ubyte b;
    uint getUint()
        {
        uint ret;
        ret+=255;
        ret<<=8;
        ret+=r;
        ret<<=8;
        ret+=g;
        ret<<=8;
        ret+=b;
        return ret;
        }
    }

void moveparticle(int x1,int y1,int x2,int y2)//@safe 
    {
    if(x1<0 || x1>=W || y1<0 || y1>=H)
        return;
    if(x2<0 || x2>=W || y2<0 || y2>=H)
        {
        deleteparticle(x1,y1);
        return;
        }

    Particle*[]* particlelist;
    switch(particlegrid[x1][y1].state)
        {
        case ParticleState.ACTIVE:
            particlelist=&active_particles;
            break;
        case ParticleState.PAUSED:
            particlelist=&paused_particles;
            break;
        case ParticleState.SLEEPING:
            particlelist=&sleeping_particles;
            break;
        default: assert(false);
        }
    
    if(!particlegrid[x2][y2].isNull)
        deleteparticle(x2,y2);
    particlegrid[x2][y2]=particlegrid[x1][y1];
    particlegrid[x2][y2].x=x2;
    particlegrid[x2][y2].y=y2;
    (*particlelist)[particlegrid[x2][y2].listpos]=&particlegrid[x2][y2];
    particlegrid[x1][y1].isNull=true;
    }
    
void setparticle(int x,int y,ParticleType particletype)//@safe 
    {
    if(x<0 || x>=W || y<0 || y>=H)
        return;
    deleteparticle(x,y);
    
    particlegrid[x][y].isNull=false;
    particlegrid[x][y].type=particletype;
    particlegrid[x][y].listpos=active_particles.length;
    particlegrid[x][y].x=x;
    particlegrid[x][y].y=y;
    particlegrid[x][y].state=ParticleState.ACTIVE;
    particlegrid[x][y].demotecount=0;
    
    active_particles.assumeSafeAppend()~=&particlegrid[x][y];
    }

void deleteparticle(int x,int y)//@safe 
    {
    if(x<0 || x>=W || y<0 || y>=H)
        return;
    if(particlegrid[x][y].isNull)
        return;

    Particle*[]* particlelist;
    switch(particlegrid[x][y].state)
        {
        case ParticleState.ACTIVE:
            particlelist=&active_particles;
            break;
        case ParticleState.PAUSED:
            particlelist=&paused_particles;
            break;
        case ParticleState.SLEEPING:
            particlelist=&sleeping_particles;
            break;
        default: assert(false);
        }
    size_t newpos=particlegrid[x][y].listpos;
    (*particlelist)[newpos]=(*particlelist)[$-1];
    (*particlelist)[newpos].listpos=newpos;
    (*particlelist).length-=1;
    particlegrid[x][y].isNull=true;
    }

void promoteparticle(int x, int y)
    {
    Particle part=particlegrid[x][y];
    if(part.isNull)
        return;
    ParticleState currentstate=part.state;
    Particle*[]* currentlist;
    ParticleState newstate;
    Particle*[]* newlist;
    
    switch(currentstate)
        {
        case ParticleState.ACTIVE:
            return;
        case ParticleState.PAUSED:
            newstate=ParticleState.ACTIVE;
            newlist=&active_particles;
            currentlist=&paused_particles;
            break;
        case ParticleState.SLEEPING:
            newstate=ParticleState.PAUSED;
            newlist=&paused_particles;
            currentlist=&sleeping_particles;
            break;
        default: assert(false);
        }
    
    particlegrid[x][y].demotecount=0;
    
    particlegrid[x][y].state=newstate;
    size_t newpos=particlegrid[x][y].listpos;
    (*currentlist)[newpos]=(*currentlist)[$-1];
    (*currentlist)[newpos].listpos=newpos;
    (*currentlist).length-=1;
    particlegrid[x][y].listpos=newlist.length;
    (*newlist)~=&particlegrid[x][y];
    }

void demoteparticle(int x, int y)
    {
    Particle part=particlegrid[x][y];
    if(part.isNull)
        return;
    ParticleState currentstate=part.state;
    Particle*[]* currentlist;
    ParticleState newstate;
    Particle*[]* newlist;
    
    switch(currentstate)
        {
        case ParticleState.ACTIVE:
            newstate=ParticleState.PAUSED;
            newlist=&paused_particles;
            currentlist=&active_particles;
            break;
        case ParticleState.PAUSED:
            newstate=ParticleState.SLEEPING;
            newlist=&sleeping_particles;
            currentlist=&paused_particles;
            break;
        case ParticleState.SLEEPING:
            return;
        default: assert(false);
        }
    
    particlegrid[x][y].demotecount+=1;
    if(particlegrid[x][y].demotecount<10)
        return;
    particlegrid[x][y].demotecount=0;
    
    particlegrid[x][y].state=newstate;
    size_t newpos=particlegrid[x][y].listpos;
    (*currentlist)[newpos]=(*currentlist)[$-1];
    (*currentlist)[newpos].listpos=newpos;
    (*currentlist).length-=1;
    particlegrid[x][y].listpos=newlist.length;
    (*newlist)~=&particlegrid[x][y];
    }

abstract class ParticleType
{
    Color color;
    bool simulate(Particle);
    
}

struct Particle
{
    bool isNull=true;
    ParticleType type;
    ParticleState state=ParticleState.ACTIVE;
    size_t listpos;
    int x;
    int y;
    int demotecount;
}
