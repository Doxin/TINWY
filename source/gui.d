private import derelict.sdl2.sdl;
private import tool;
private import settings;
private import particle;
private import io=std.stdio;

void gui_mouse_event(SDL_Event event)
    {
    if(event.type!=SDL_MOUSEBUTTONUP)
        return;

    int newtool=event.button.x/TOOLBARH;
    
    if(newtool>=toollist.length || newtool<0)
        return;
    
    if(event.button.button==SDL_BUTTON_LEFT)
        primary_tool=newtool;
    else if(event.button.button==SDL_BUTTON_RIGHT)
        secondary_tool=newtool;
    }

void gui_render(SDL_Renderer *renderer)
    {
    //io.writeln(toollist);
    for(int i=0;i<toollist.length;i++)
        {
        Tool tool=toollist[i];
        Color color=tool.color;
        int x=i*TOOLBARH;
        SDL_Rect rect={x,H,TOOLBARH,TOOLBARH};
        renderer.SDL_SetRenderDrawColor(color.r,color.g,color.b,255);
        renderer.SDL_RenderFillRect(&rect);
        
        if(primary_tool==i)
            {
            SDL_Rect rect1={x+1,H+1,TOOLBARH-2,TOOLBARH-2};
            renderer.SDL_SetRenderDrawColor(255,0,0,255);
            renderer.SDL_RenderDrawRect(&rect1);
            }
        
        if(secondary_tool==i)
            {
            SDL_Rect rect2={x+3,H+3,TOOLBARH-6,TOOLBARH-6};
            renderer.SDL_SetRenderDrawColor(0,0,255,255);
            renderer.SDL_RenderDrawRect(&rect2);
            }
        }
    }
