private import settings;
private import particle;
private import std.random;
private import tool;

ParticleType pt_dust;

static this()
    {
    pt_dust=new DustParticle;
    toollist~=new DustTool;
    }

class DustTool:ParticleTool
{
    this()
        {
        this.particletype=new DustParticle;
        this.color=this.particletype.color;
        }
}

class DustParticle:ParticleType
{
    this()
        {
        color.r=255;
        color.g=224;
        color.b=160;
        }
    override bool simulate(Particle part)
    {
    if(part.y>=H-1)
        {
        deleteparticle(part.x,part.y);
        return false;
        }
    if(particlegrid[part.x][part.y+1].isNull)
        moveparticle(part.x,part.y,part.x,part.y+1);
    else if(uniform(0,101)<=50)
        {
        if(particlegrid[part.x+1][part.y+1].isNull)
            moveparticle(part.x,part.y,part.x+1,part.y+1);
        else if(particlegrid[part.x-1][part.y+1].isNull)
            moveparticle(part.x,part.y,part.x-1,part.y+1);
        }
    else if(particlegrid[part.x-1][part.y+1].isNull)
        moveparticle(part.x,part.y,part.x-1,part.y+1);
    else if(particlegrid[part.x+1][part.y+1].isNull)
        moveparticle(part.x,part.y,part.x+1,part.y+1);
    else
        return false;
    return true;
    }
}
