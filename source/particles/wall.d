private import io=std.stdio;
private import particle;
private import tool;

ParticleType pt_wall;

static this()
    {
    pt_wall=new WallParticle;
    toollist~=new WallTool;
    }

class WallTool:ParticleTool
{
    this()
        {
        this.particletype=new WallParticle;
        this.color=this.particletype.color;
        }
}


class WallParticle:ParticleType
{
    this()
        {
        color.r=128;
        color.g=128;
        color.b=128;
        }
    override bool simulate(Particle part)
    {
        return false;
    }
}
