private import particle;

Tool[] toollist;
int primary_tool=1;
int secondary_tool=0;
int toolradius=8;

abstract class Tool
    {
    Color color;
    void apply_pixel(int x,int y);
    }

abstract class ParticleTool:Tool
    {
    ParticleType particletype;
    override void apply_pixel(int x,int y)
        {
        setparticle(x,y,particletype);
        }
    }
