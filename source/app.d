private import io=std.stdio;
private import derelict.sdl2.sdl;
private import settings;
private import particle;
private import std.exception;
private import gui;
private import tool;
private import math=std.math;

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Texture *screen_tex;

int loopcount;
bool primarybutton=false;
bool secondarybutton=false;

int mousex;
int mousey;
int pmousex;
int pmousey;

double distance(double x1,double y1,double x2,double y2)
    {
    return math.sqrt(math.pow(x2-x1,2)+math.pow(y2-y1,2));
    }

int mainloop(bool renderframe)
{
    loopcount+=1;
    
    // --- handle events ---
    if(renderframe)
        {
        SDL_Event event;
        while(SDL_PollEvent(&event))
            {
            switch(event.type)
                {
                case SDL_QUIT:
                    return 1;
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                    if(event.button.y>H)
                        gui_mouse_event(event);
                    else
                        {
                        if(event.button.button==SDL_BUTTON_LEFT)
                            primarybutton=(event.button.state==SDL_PRESSED);
                        else if(event.button.button==SDL_BUTTON_RIGHT)
                            secondarybutton=(event.button.state==SDL_PRESSED);
                        }
                    break;
                default:
                }
            }
        }
    
    // --- apply tools ---
    pmousex=mousex;
    pmousey=mousey;
    SDL_GetMouseState(&mousex,&mousey);
    int tot=cast(int)math.fmax(1,cast(int)distance(pmousex,pmousey,mousex,mousey));
    for(int r=tot;r>0;r--)
        {
        double perc=(cast(double)r)/(cast(double)tot);
        int rmx=cast(int)(((cast(double)pmousex)*(1.0-perc))+((cast(double)mousex)*perc));
        int rmy=cast(int)(((cast(double)pmousey)*(1.0-perc))+((cast(double)mousey)*perc));
        for(int i=0;i<=1;i++)
            {
            int toolnum;
            if(i==0 && primarybutton)
                toolnum=primary_tool;
            else if(i==1 && secondarybutton)
                toolnum=secondary_tool;
            else
                continue;
            for(int x=rmx-toolradius;x<rmx+toolradius;x++)
                for(int y=rmy-toolradius;y<rmy+toolradius;y++)
                    if(distance(rmx,rmy,x,y)<=toolradius)
                        toollist[toolnum].apply_pixel(x,y);
            }
        }
    
    // --- reset screen to black ---
    
    if(renderframe)
        screen[]=0;
    
    // --- simulate ---
    
    foreach(part; active_particles)
        {
        assert(!part.isNull,"active particle is null");
        assert(particlegrid[part.x][part.y].x==part.x,"x mismatch");
        assert(particlegrid[part.x][part.y].y==part.y,"y mismatch");
        if(part.type.simulate(*part))
            promoteparticle(part.x,part.y);
        else
            demoteparticle(part.x,part.y);
        assert(particlegrid[part.x][part.y].x==part.x,"x mismatch");
        assert(particlegrid[part.x][part.y].y==part.y,"y mismatch");
        }
    
    if(loopcount%2==0)
        foreach(part; paused_particles)
            {
            assert(!part.isNull,"paused particle is null");
            assert(particlegrid[part.x][part.y].x==part.x,"x mismatch");
            assert(particlegrid[part.x][part.y].y==part.y,"y mismatch");
            if(part.type.simulate(*part))
                promoteparticle(part.x,part.y);
            else
                demoteparticle(part.x,part.y);
            assert(particlegrid[part.x][part.y].x==part.x,"x mismatch");
            assert(particlegrid[part.x][part.y].y==part.y,"y mismatch");
            }
    
    if(loopcount%10==5)
        foreach(part; sleeping_particles)
            {
            assert(!part.isNull,"sleeping particle is null");
            assert(particlegrid[part.x][part.y].x==part.x,"x mismatch");
            assert(particlegrid[part.x][part.y].y==part.y,"y mismatch");
            if(part.type.simulate(*part))
                promoteparticle(part.x,part.y);
            else
                demoteparticle(part.x,part.y);
            assert(particlegrid[part.x][part.y].x==part.x,"x mismatch");
            assert(particlegrid[part.x][part.y].y==part.y,"y mismatch");
            }
    
    // --- render ---
    
    if(renderframe)
        {
        foreach(part; active_particles)
            {
            screen[part.x+part.y*W]=part.type.color.getUint();
            }
        foreach(part; paused_particles)
            {
            screen[part.x+part.y*W]=part.type.color.getUint();
            }
        foreach(part; sleeping_particles)
            {
            screen[part.x+part.y*W]=part.type.color.getUint();
            }
        }

    // --- update texture from screen array, and blit to screen ---
    if(renderframe)
        {
        SDL_UpdateTexture (screen_tex,null,cast(void*)screen,cast(int)(W*uint.sizeof));
        screen_tex.SDL_SetTextureBlendMode(SDL_BLENDMODE_NONE);
        SDL_Rect rects={0,0,W,H};
        const(SDL_Rect)* rect=&rects;
        renderer.SDL_RenderCopy(screen_tex,rect,rect);
            
        renderer.SDL_RenderPresent();
        }
    
    // --- render GUI ---
    
    gui_render(renderer);
    
    return 0;
}

int do_main()
{
    int starttime;
    int endtime;
    int timebudget;
    int framecount;
    int stepcount;
    int r;

	DerelictSDL2.load();
	SDL_Init(SDL_INIT_VIDEO);
    window=SDL_CreateWindow("title",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,W,H+TOOLBARH,0);
    if(window==null)
        {
        io.writeln("error opening SDL window");
        return -1;
        }
    renderer=SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if(renderer==null)
        {
        io.writeln("error creating SDL renderer");
        return -1;
        }

    screen_tex=renderer.SDL_CreateTexture(SDL_PIXELFORMAT_ARGB8888,SDL_TEXTUREACCESS_STATIC,W,H);

    while(true)
        {
        timebudget=FRAMETIME;
        r=mainloop(true);//run mainloop and render to screen
        if(r==1)
            return 0;
        else if(r!=0)
            return r;
        
        starttime=SDL_GetTicks();
        endtime=starttime;
        while((timebudget-(endtime-starttime))>0)
            {
            r=mainloop(false);//only simulate until we run out of framebudget
            if(r==1)
                return 0;
            else if(r!=0)
                return r;
            stepcount+=1;
            endtime=SDL_GetTicks();
            }
        
        framecount+=1;
        if(framecount>=30)
            {
            io.writeln(cast(int)(cast(float)stepcount)/(cast(float)framecount)," steps per frame.");
            io.writeln(active_particles.length+paused_particles.length+sleeping_particles.length," particles");
            framecount=0;
            stepcount=0;
            }
        }
}

int main()
{
    int r;
    r=do_main();
    SDL_Quit();
    return r;
}
