private import tool;
private import particle;

static this()
    {
    toollist~=new DeleteTool;
    }

class DeleteTool:Tool
{
    override void apply_pixel(int x,int y)
        {
        deleteparticle(x,y);
        }
}
